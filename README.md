#talent-validate(js验证框架)
<h3>演示地址：<a href='http://www.t-io.org/talent-validate/' target='_blank'>http://www.t-io.org/talent-validate/</a></h3>
一 、框架的一些特点：
1、易用是必须的，下载代码后，打开index.html，有使用手册和各种例子
2、灵活和强大是必须的。几乎适用所有的页面验证，尤其是复杂灵活的页面验证，譬如当A条件成立时，需要验证Field1为邮箱，当A条件不成立时，需要验证Field1为手机号，本框架都轻松应付。

二、框架历史
1、2006年在公司内部开发的，当时的JS水平，你懂的
2、2007年重写，但并未在公司项目中使用
3、2010年在公司项目中大量使用、在个人博客上发布根据网友反馈持续优化
4、2014年第一次在面向大众的互联网产品中使用: www.51rebo.cn
5、2016年重写使用手册、发布到码云(由于在开发www.biiiu.cn时，前端工程师选的验证框架难以满足项目需求而耽误了4天工时，所以本人再次感受到前端验证的重要性)

三、使用步骤： 请打开index.html，在左侧树中找到“快速入门”
<br>
<img src='https://git.oschina.net/tywo45/talent-validate/raw/master/img/demo.png?dir=0&filepath=img%2Fdemo.png&oid=c2fef1e423c8f1db616d6da8cc556fe153438b74&sha=38db9c6d2edb81b8224919fc55c4fa8ed7884b01'></img>